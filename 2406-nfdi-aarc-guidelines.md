---
# vim:tw=100:ft=markdown
author: Marcus Hardt

title: <smaller>AARC Guidelines relevant for NFDI</smaller>
<!--Integration of Infrastructure Capacity in EOSC: policy & technical overview-->
date: Jun 2024
theme: marcus2
parallaxBackgroundImage: images/nfdi-bg-slide.png
<!-- title-slide-attributes: -->
<!--     data-background-image: images/nfdi-bg-head.png -->
slideNumber: \'c/t\'
preloadIframes: true
pdfSeparateFragments: false
pdfMaxPagesPerSlide: 1
showNotes: false
mouseWheel: true
<!-- transition: none -->
<!--backgroundTransition: none-->

<!--REMOTE_USER: presentations-->
<!--REMOTE_HOST: cvs.data.kit.edu-->
<!--REMOTE_URL: https://infra.eosc-synergy.eu/~presentations/2106-Synergy-Review-WP2-->
<!--REVEAL_DIR: public_html/reveal.js-->
<!--REVEAL_URL: /~presentations/reveal.js-->

---

## Disclaimers

- This is likely biased by my own view
- This is likely **impaired** by my own view
- This is probably incomplete

But it should be a good start


# AARC Guidelines

## Background

- Guidelines are approved by:  <br/>
 **[AEGIS](https://wiki.geant.org/display/AARC/Guidelines+Adoption+Status)**
    "AARC Engagement Group for InfrastructureS"
- <https://wiki.geant.org/display/AARC/Guidelines+Adoption+Status>

## AEGIS Members

AEGIS is a group of Infrastructure representatives

<div class="columns">
  <div class="column">
- Members:
    - GÉANT
    - EUDAT
    - EGI
    - ELIXIR
    - ACCESS
    - DARIAH
    - Life Science AAI
    - UmbrellaID
    - HIFIS
</div>
<div class="column">
- Observers
    - CERN
    - FENIX RI
    - Internet2
    - WLCG
    - IRIS/STFC
    - SURF Research Access Management
    - LUMI / PUHURI
    - Australian Access Federation
    - Digital Research Alliance of Canada
    - Global Open Science Cloud
    - National Institute of Informatics, Japan (NII)
</div>
 </div>


# Guidelines
## Overview

| Number   | Name                                                                      |
|----------|---------------------------------------------------------------------------|
| ~~G002~~ | ~~Guidelines on expressing **group membership** and role information~~    |
| G021     | Exchange of specific **assurance** information between Infrastructure     |
| G025     | Guidelines for expressing **affiliation information**                     |
| G026     | Guidelines for expressing **community user identifiers**                  |
| G027     | Specification for expressing **resource capabilities**                    |
| G031     | Guidelines for evaluating the combined assurance of **linked identities** |
| G045     | AARC Blueprint Architecture 2019                                          |
| ~~G049~~ | ~~A specification for **IdP hinting**~~                                   |
| G057     | Inferring and constructing **voPersonExternalAffiliation**                |
| G061     | A specification for **IdP hinting** (obsoletes AARC-G049)                 |
| G062     | A specification for **hinting** an IdP which **discovery service** to use |
| G063     | A specification for **providing information about an end service**        |
| G069     | Expressing **group and role information** (supersedes AARC-G002)          |
| G071     | Guidelines for **Secure Operation of Attribute Authorities**              |


## <small> G021 - Exchange of specific **assurance** information between Infrastructure</small>

- Spec: <https://aarc-community.org/guidelines/aarc-g021>
- Builds on the REFEDS Assurance Framework (RAF) <https://refeds.org/assurance>
    - Identifier uniqueness `ID: [unique|eppn-no-reassign]`
    - Identity Vetting `IAP: [low|medium|high|local-enterprise]`
    - Attribute Freshness `ATP: [ePA-1m|ePA-1d]`
    - Profiles: https://refeds.org/assurance/profile/espresso/[cappuccino|espresso]
- G021 adds profiles, that define more components (i.e. MFA):
    - https://igtf.net/ap/authn-assurance/birch
    - https://igtf.net/ap/authn-assurance/dogwood
    - https://aarc-project.eu/policy/authn-assurance/assam

## <small> G021 - Exchange of specific **assurance** information between Infrastructure</small>

<img width="700" alt="line-numbers" src="images/assurance-profiles.png">


## <small> G025 - Guidelines for expressing **affiliation information**</small>

- Spec: <https://aarc-community.org/guidelines/aarc-g025>
- Types of affiliation
    - Affiliation within Home Organisation
        - `eduPersonScopedAffiliation` => `voPersonExternalAffiliation`
    - Affiliation within Community
        - New `eduPersonScopedAffiliation`
- Complements `assurance` definition
    - `$AARC-PREFIX$=https://aarc-community.org/assurance`
    - `$AARC-PREFIX$/ATP/ePA-1m`
    - `$AARC-PREFIX$/ATP/ePA-1d`
    - `$AARC-PREFIX$/ATP/vPEA-1m`
    - `$AARC-PREFIX$/ATP/vPEA-1d`

## <small> G025 - Guidelines for expressing **affiliation information**</small>

<img width="500" alt="line-numbers" src="images/g025.png">


## <small> G026 - Guidelines for expressing **community user identifiers**</small>

- Spec: <https://aarc-community.org/guidelines/aarc-g026>
- Identifier Concepts:
    - persistent / permanent / opaque / targeted (or pairwise)
- Identifiers
    - **MUST** be unique within the issuing system
    - **MUST** not collide
    - **SHOULD** be opaque
    - **MUST NOT** be reassigned
    - **SHOULD** be permanent
    - **MUST** be persistent
    - **MUST** be shared (unless privacy and regulatory requirements need to be met)
- SAML
    - **MUST** use `voPersonID` (voPerson-V2.0)
    - Don't use `saml:NameID`
- OIDC
    - `voperson_id`: **MUST** be in `userinfo` and `ID Token`


## <small> G027 - Specification for expressing **resource capabilities**</small>

- Spec: <https://aarc-community.org/guidelines/aarc-g027>
- `<NAMESPACE>:res:<RESOURCE>[:<CHILD-RESOURCE>]...[:act:<ACTION>[,<ACTION>]...]#<AUTHORITY>`
- Examples:

    ```
    urn:example:example-ri.org:res:example-res.org#auth-x.example-infra-b.org

    urn:example:example-ri.org:\
        res:vm_dashboard:storage:act:create,delete\
        #auth-x.example-ri.org

    urn:example:example-ri.org:res:database:act:SQL_USER_LIST_STATEMENT\
        #auth-x.example-infra.org
    ```


## <small> G031 - Guidelines for evaluating the combined assurance of **linked identities**</small>

- Spec: <https://aarc-community.org/guidelines/aarc-g031>
- Identifies combined assurance per "component"
    - ID, IAP, ATP
- Too complex for now


## <small> G045 - AARC Blueprint Architecture 2019</small>

- Spec: <https://aarc-community.org/guidelines/aarc-g045>

## <small> G045 - AARC Blueprint Architecture 2019</small>

<img width="600" alt="line-numbers" src="images/bpa.png">

## <small> G045 - AARC Blueprint Architecture 2019</small>

<img width="500" alt="line-numbers" src="images/bpa2019.png">


## <small> G057 - Inferring and constructing **voPersonExternalAffiliation**</small>

- Spec: <https://aarc-community.org/guidelines/aarc-g057>
- When to construct origin affiliation (=> be private)
- How to construct origin affiliation
    - Short and concise instructions


## <small> G061 - A specification for **IdP hinting** (obsoletes AARC-G049)</small>

- Spec: <https://aarc-community.org/guidelines/aarc-g061>
- Rationale: SP can ask Proxy to use a particular IdP without asking user
- Use cases:
    - Reauthentication
    - VO Service Page
- Specifies `url` parameter: `aarc_idp_hint`
    - (At least one) url-encoded `entity identifier` or `OIDC-OP`
    - **MUST** point to an SP-IdP-Proxy or Home-IdP
    - **MAY** be nested


## <small> G062 - A specification for **hinting** an IdP which **discovery service** to use</small>

- Spec: <https://aarc-community.org/guidelines/aarc-g062>
- Use cases:
    - Use specifically branded discovery
    - Use discovery with smaller set of IdPs
- Specifies `url` parameter: `aarc_ds_hint`
    - Single url-encoded `entity identifier`
    - `aarc_idp_hint` has precedence


## <small> G063 - A specification for **providing information about an end service**</small>

- Spec: <https://aarc-community.org/guidelines/aarc-g063>
- Use cases:
    - SP passes a `service identifier` to a DS, potentially through the proxy.
- Specifies `url` parameter: `aarc_service_hint`

## <small> G069 - Expressing **group and role information** (supersedes AARC-G002)</small>

- Spec: <https://aarc-community.org/guidelines/aarc-g069>
- Fixes problems of G002
    - Unicode & Co
    - Regular expressions
- <bitsmall>`<NAMESPACE>:group:<GROUP>[:<SUBGROUP>*][:role=<ROLE>][#<AUTHORITY>]`</bitsmall>
- Examples:

    ```
    ^urn:example:foo:group:parentgroup(:[^:#]+)*(:role=[^:#]+)?(#.+)?$

        urn:example:foo:group:parentgroup
        urn:example:foo:group:parentgroup:role=manager
        urn:example:foo:group:parentgroup#authority
        urn:example:foo:group:parentgroup:role=manager#authority
        urn:example:foo:group:parentgroup:childgroup:role=manager
        urn:example:foo:group:parentgroup:childgroup:grandchildgroup:role=manager

    ^urn:example:foo:group(:[^:]+)+:role=myrole(#.+)?$

    ^urn:example:foo:group(:[^:]+)*:mygroup(:[^:]+)*(:role=[^:#]+)?(#.+)?$

   ```


## <small> G071 - Guidelines for **Secure Operation of Attribute Authorities**</small>

- Spec: <https://aarc-community.org/guidelines/aarc-g071>
- Long ;)
- Read it yourself
- We need to run the exercise using the "self-assessment review sheet":<br/>
    <small>
    <https://docs.google.com/spreadsheets/d/1zs6fhf-nDTmlTVa89X7GVzPTHWwuaFCf/edit#gid=2072996767>
    </small>


## <small> G052 - Guidelines for **Proxied Token Introspection**</small>

- Spec: https://aarc-community.org/guidelines/aarc-g052/
- Goal: Use CAAI AT at service connected to an Infra-Proxy
    - Relevant for both classes of Infra-Proxies: 
        - Actual Infra Proxy
        - Site-Local Infra Proxy
- Mechanism:
    - A CAAI issues a JWT AT to a client
    - That client sends this token to a service (a client of the Infra Proxy)
    - Notes:
        - The service has no relation with CAAI
        - The service sends any token for introspection to his Infra Proxy
    - If the Infra Proxy supports G052, it forwards it JWT-AT to the `iss` found in the token
    - Infra Proxy forwards the answer from the `iss`
        - Infra Proxy may "adjust" claims


# {data-background-image="images/nfdi-bg-end.png" data-background-size="contain" }

